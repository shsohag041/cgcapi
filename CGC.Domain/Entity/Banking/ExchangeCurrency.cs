﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGC.Domain.Entity.Banking
{
    public class ExchangeCurrency:BaseEntity
    {
        public string FromCurrency { get; set; } = string.Empty;
        public string ToCurrency { get; set; }= string.Empty;
        public string UserId { get; set; }=string.Empty;
        public double Price { get; set; }
    }
}
