﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGC.Domain.Entity.Banking
{
    public class ExchangeCardHistory:BaseEntity
    {
        public string? ExchangeCardId { get; set; } = string.Empty;
        public string? UserId { get; set; } = string.Empty;
        public double? Price { get; set; } = 0;
        public string? BankId { get; set; } = string.Empty;
        public string? BankName { get; set; } = string.Empty;
        public string? AccountNumber { get; set;} = string.Empty;
        public int Status { get; set; }=1;
    }
}
