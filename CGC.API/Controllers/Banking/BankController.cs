﻿using CGC.Application.IService;
using CGC.Application.IService.Banking;
using CGC.Domain.Entity.Banking;
using CGC.Domain.Entity.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CGC.API.Controllers.Banking
{
    [Route("api/[controller]")]
    [ApiController]
    public class BankController : ControllerBase
    {
        private readonly IBankService _service;
        public BankController(IBankService service)
        {
            _service = service;
        }
        [HttpGet("GetAll")]
        public async Task<IEnumerable<Bank>> GetAll()
        {
            var list = await _service.GetBankList();

            return list;
        }
        [HttpGet("GetById")]
        public Bank GetById(string id)
        {
            var list = _service.GetBankById(id);

            return list;
        }
        [HttpPost("Add")]
        public async Task<IActionResult> Add([FromBody] Bank obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            await _service.AddBank(obj);

            return Ok(obj);

        }
        [HttpPut("Update")]
        public async Task Update(Bank obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.UpdateBank(obj);


        }
        [HttpDelete("Delete")]
        public async Task Delete(Bank obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            await _service.DeleteBank(obj);

        }
    }
}
