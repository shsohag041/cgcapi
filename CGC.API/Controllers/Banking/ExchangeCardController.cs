﻿using CGC.Application.IService.Banking;
using CGC.Domain.Entity.Banking;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CGC.API.Controllers.Banking
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExchangeCardController : ControllerBase
    {
        private readonly IExchangeCardService _service;
        public ExchangeCardController(IExchangeCardService service)
        {
            _service = service;
        }
        [HttpGet("GetAll")]
        public async Task<IEnumerable<ExchangeCard>> GetAll()
        {
            var list = await _service.GetExchangeCardList();

            return list;
        }
        [HttpGet("GetById")]
        public ExchangeCard GetById(string id)
        {
            var list = _service.GetExchangeCardById(id);

            return list;
        }
        [HttpPost("Add")]
        public async Task<IActionResult> Add([FromBody] ExchangeCard obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.AddExchangeCard(obj);
            return Ok(obj);
        }
        [HttpPut("Update")]
        public async Task Update(ExchangeCard obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.UpdateExchangeCard(obj);
        }
        [HttpDelete("Delete")]
        public async Task Delete(ExchangeCard obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.DeleteExchangeCard(obj);

        }
    }
}
