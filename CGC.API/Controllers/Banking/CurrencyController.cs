﻿using CGC.Application.IService.Banking;
using CGC.Domain.Entity.Banking;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CGC.API.Controllers.Banking
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        private readonly ICurrencyService _service;
        public CurrencyController(ICurrencyService service)
        {
            _service = service;
        }
        [HttpGet("GetAll")]
        public async Task<IEnumerable<Currency>> GetAll()
        {
            var list = await _service.GetCurrencyList();

            return list;
        }
        [HttpGet("GetById")]
        public Currency GetById(string id)
        {
            var list = _service.GetCurrencyById(id);

            return list;
        }
        [HttpPost("Add")]
        public async Task<IActionResult> Add([FromBody] Currency obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            await _service.AddCurrency(obj);

            return Ok(obj);

        }
        [HttpPut("Update")]
        public async Task Update(Currency obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.UpdateCurrency(obj);


        }
        [HttpDelete("Delete")]
        public async Task Delete(Currency obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            await _service.DeleteCurrency(obj);

        }
    }
}
