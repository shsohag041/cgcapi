﻿using CGC.Application.IService.Banking;
using CGC.Domain.Entity.Banking;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CGC.API.Controllers.Banking
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyToCGCController : ControllerBase
    {
        private readonly ICurrencyToCGCService _service;
        public CurrencyToCGCController(ICurrencyToCGCService service)
        {
            _service = service;
        }
        [HttpGet("GetAll")]
        public async Task<IEnumerable<CurrencyToCGC>> GetAll()
        {
            var list = await _service.GetCurrencyToCGCList();

            return list;
        }
        [HttpGet("GetById")]
        public CurrencyToCGC GetById(string id)
        {
            var list = _service.GetCurrencyToCGCById(id);

            return list;
        }
        [HttpPost("Add")]
        public async Task<IActionResult> Add([FromBody] CurrencyToCGC obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            await _service.AddCurrencyToCGC(obj);

            return Ok(obj);

        }
        [HttpPut("Update")]
        public async Task Update(CurrencyToCGC obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.UpdateCurrencyToCGC(obj);


        }
        [HttpDelete("Delete")]
        public async Task Delete(CurrencyToCGC obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            await _service.DeleteCurrencyToCGC(obj);

        }
    }
}
