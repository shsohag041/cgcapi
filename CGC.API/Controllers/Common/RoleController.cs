﻿using CGC.Application.IService;
using CGC.Domain.Entity.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CGC.API.Controllers.Common
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _service;
        public RoleController(IRoleService service)
        {
            _service = service;
        }
        [HttpGet("GetAll")]
        public async Task<IEnumerable<Role>> GetAll()
        {
            var list = await _service.GetRoleList();

            return list;
        }
        [HttpGet("GetById")]
        public Role GetById(string id)
        {
            var list = _service.GetRoleById(id);

            return list;
        }
        [HttpPost("Add")]
        public async Task<IActionResult> Add([FromBody] Role obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            await _service.AddRole(obj);

            return Ok(obj);

        }
        [HttpPut("Update")]
        public async Task Update(Role obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.UpdateRole(obj);


        }
        [HttpDelete("Delete")]
        public async Task Delete(Role obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            await _service.DeleteRole(obj);

        }
    }
}
