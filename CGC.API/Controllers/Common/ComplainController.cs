﻿using CGC.Application.IService;
using CGC.Domain.Entity.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CGC.API.Controllers.Common
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComplainController : ControllerBase
    {
        private readonly IComplainService _service;
        public ComplainController(IComplainService service)
        {
            _service = service;
        }
        [HttpGet("GetAll")]
        public async Task<IEnumerable<Complain>> GetAll()
        {
            var list = await _service.GetComplainList();

            return list;
        }
        [HttpGet("GetById")]
        public Complain GetById(string id)
        {
            var list = _service.GetComplainById(id);

            return list;
        }
        [HttpPost("Add")]
        public async Task<IActionResult> Add([FromBody] Complain obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            await _service.AddComplain(obj);

            return Ok(obj);

        }
        [HttpPut("Update")]
        public async Task Update(Complain obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.UpdateComplain(obj);


        }
        [HttpDelete("Delete")]
        public async Task Delete(Complain obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.DeleteComplain(obj);
        }
    }
}
