﻿using CGC.Application.IService;
using CGC.Domain.Entity.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CGC.API.Controllers.Common
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuPermissionController : ControllerBase
    {
        private readonly IMenuPermissionService _service;
        public MenuPermissionController(IMenuPermissionService service)
        {
            _service = service;
        }
        [HttpGet("GetAll")]
        public async Task<IEnumerable<MenuPermission>> GetAll()
        {
            var list = await _service.GetMenuPermissionList();

            return list;
        }
        [HttpGet("GetById")]
        public MenuPermission GetById(string id)
        {
            var list = _service.GetMenuPermissionById(id);

            return list;
        }
        [HttpPost("Add")]
        public async Task<IActionResult> Add([FromBody] MenuPermission obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            await _service.AddMenuPermission(obj);

            return Ok(obj);

        }
        [HttpPut("Update")]
        public async Task Update(MenuPermission obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.UpdateMenuPermission(obj);


        }
        [HttpDelete("Delete")]
        public async Task Delete(MenuPermission obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            await _service.DeleteMenuPermission(obj);

        }
    }
}
