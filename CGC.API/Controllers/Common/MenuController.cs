﻿using CGC.Application.IService;
using CGC.Domain.Entity.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CGC.API.Controllers.Common
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly IMenuService _service;
        public MenuController(IMenuService service)
        {
            _service = service;
        }
        [HttpGet("GetAll")]
        public async Task<IEnumerable<Menu>> GetAll()
        {
            var list = await _service.GetMenuList();

            return list;
        }
        [HttpGet("GetById")]
        public Menu GetById(string id)
        {
            var list = _service.GetMenuById(id);

            return list;
        }
        [HttpPost("Add")]
        public async Task<IActionResult> Add([FromBody] Menu obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            await _service.AddMenu(obj);

            return Ok(obj);

        }
        [HttpPut("Update")]
        public async Task Update(Menu obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.UpdateMenu(obj);


        }
        [HttpDelete("Delete")]
        public async Task Delete(Menu obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.DeleteMenu(obj);

        }
    }
}
