﻿using CGC.Application.IService;
using CGC.Domain.Entity.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CGC.API.Controllers.Common
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComplainRemarkController : ControllerBase
    {
        private readonly IComplainRemarkService _service;
        public ComplainRemarkController(IComplainRemarkService service)
        {
            _service = service;
        }
        [HttpGet("GetAll")]
        public async Task<IEnumerable<ComplainRemark>> GetAll()
        {
            var list = await _service.GetComplainRemarkList();

            return list;
        }
        [HttpGet("GetById")]
        public ComplainRemark GetById(string id)
        {
            var list = _service.GetComplainRemarkById(id);

            return list;
        }
        [HttpPost("Add")]
        public async Task<IActionResult> Add([FromBody] ComplainRemark obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            await _service.AddComplainRemark(obj);

            return Ok(obj);

        }
        [HttpPut("Update")]
        public async Task Update(ComplainRemark obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.UpdateComplainRemark(obj);


        }
        [HttpDelete("Delete")]
        public async Task Delete(ComplainRemark obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            await _service.DeleteComplainRemark(obj);

        }
    }
}
